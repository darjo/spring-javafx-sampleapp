package pl.daprog.springjavafx.client.customer;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;

import javax.inject.Inject;

import pl.daprog.springjavafx.app.server.pub.CustomerDto;
import pl.daprog.springjavafx.app.server.pub.CustomerService;
import pl.daprog.springjavafx.client.main.MainPresenter;

public class CustomerDetailPresenter {
	
	@FXML
	private Node root;
	@FXML
	private TextField firstNameField;
	@FXML
	private TextField lastNameField;

	@Inject
	private CustomerService customerService;
	@Inject
	private MainPresenter mainPresenter;

	private Long customerId;

	public Node getView() {
		return root;
	}

	public void setCustomer(final Long customerId) {
		
		this.customerId = customerId;
		firstNameField.setText("");
		lastNameField.setText("");
		
		final Task<CustomerDto> loadTask = new Task<CustomerDto>() {
			protected CustomerDto call() throws Exception {
				if(customerId!=null) {
					return customerService.find(customerId);
				} else {
					CustomerDto customer = new CustomerDto();
					return customerService.init(customer);
				}
			}
		};

		loadTask.stateProperty().addListener(
				new ChangeListener<Worker.State>() {
					public void changed(
							ObservableValue<? extends Worker.State> source,
							Worker.State oldState, Worker.State newState) {
						if (newState.equals(Worker.State.SUCCEEDED)) {
							CustomerDto customer = loadTask.getValue();
							firstNameField.setText(customer.name);
							
						}
					}
				});

		new Thread(loadTask).start();
	}

	@FXML
	public void cancel(ActionEvent event) {
		
		mainPresenter.showCustomerSearch();
	}

	@FXML
	public void save(ActionEvent event) {
		
		final CustomerDto updatedCustomer = new CustomerDto();
		updatedCustomer.id = customerId;
		updatedCustomer.name = firstNameField.getText();

		final Task<CustomerDto> saveTask = new Task<CustomerDto>() {
			protected CustomerDto call() throws Exception {
				
				if(updatedCustomer.id==null) {
					customerService.insert(updatedCustomer);
					return null;
				} else {
					return customerService.update(updatedCustomer);
				}
			}
		};

		saveTask.stateProperty().addListener(
				new ChangeListener<Worker.State>() {
					public void changed(
							ObservableValue<? extends Worker.State> source,
							Worker.State oldState, Worker.State newState) {
						if (newState.equals(Worker.State.SUCCEEDED)) {
							mainPresenter.showCustomerSearch();
						}
					}
				});

		new Thread(saveTask).start();
	}
}