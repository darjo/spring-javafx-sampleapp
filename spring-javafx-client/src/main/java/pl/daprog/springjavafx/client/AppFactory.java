package pl.daprog.springjavafx.client;

import java.io.IOException;

import javafx.fxml.FXMLLoader;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.httpinvoker.SimpleHttpInvokerRequestExecutor;

import pl.daprog.springjavafx.app.server.pub.CustomerService;
import pl.daprog.springjavafx.client.customer.CustomerDetailPresenter;
import pl.daprog.springjavafx.client.customer.CustomerListPresenter;
import pl.daprog.springjavafx.client.main.MainPresenter;

@Configuration
public class AppFactory {
	
    private static Object createClientService(Class clazz) {
    	
    	// convert interface name to url
    	String serviceUrl = clazz.getSimpleName();
    	serviceUrl = serviceUrl.substring(0,1).toLowerCase() + serviceUrl.substring(1);
    	serviceUrl = serviceUrl.replace("Service", ".service");
    	
    	// create httpinvokerproxy 
        HttpInvokerProxyFactoryBean factory = new HttpInvokerProxyFactoryBean();
        String serverUrl = "http://localhost:8080/spring-javafx-server/"+serviceUrl;
        factory.setServiceUrl(serverUrl);
        factory.setServiceInterface(clazz);
        factory.setHttpInvokerRequestExecutor(new SimpleHttpInvokerRequestExecutor());
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public CustomerService customerService()
    {
        return (CustomerService) AppFactory.createClientService(CustomerService.class);
    }


    @Bean
    public MainPresenter mainPresenter()
    {
        return loadPresenter("/fxml/Main.fxml");
    }

    @Bean
    public CustomerListPresenter customerListPresenter()
    {
        return loadPresenter("/fxml/CustomerSearch.fxml");
    }

    @Bean
    public CustomerDetailPresenter contactDetailPresenter()
    {
        return loadPresenter("/fxml/CustomerDetail.fxml");
    }

    private <T> T loadPresenter(String fxmlFile)
    {
        try
        {
            FXMLLoader loader = new FXMLLoader();
            loader.load(getClass().getResourceAsStream(fxmlFile));
            return (T) loader.getController();
        }
        catch (IOException e)
        {
            throw new RuntimeException(String.format("Unable to load FXML file '%s'", fxmlFile), e);
        }
    }
}
