package pl.daprog.springjavafx.client.main;

import javax.inject.Inject;

import pl.daprog.springjavafx.client.customer.CustomerDetailPresenter;
import pl.daprog.springjavafx.client.customer.CustomerListPresenter;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

public class MainPresenter {

	@FXML
	private Parent root;
	@FXML
	private BorderPane contentArea;

	@Inject
	private CustomerListPresenter customerListPresenter;

	@Inject 
	private CustomerDetailPresenter customerDetailPresenter;

	public Parent getView() {
		return root;
	}

	public void showCustomerSearch() {
		customerListPresenter.search(null);
		contentArea.setCenter(customerListPresenter.getView());
	}
	
	 public void showCustomerDetail(Long id) {
		 customerDetailPresenter.setCustomer(id);
		 contentArea.setCenter(customerDetailPresenter.getView()); 
	  }
	 
}
