package pl.daprog.springjavafx.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.daprog.springjavafx.client.main.MainPresenter;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args)
    {
        launch(args);
    }
    
	@Override
	public void start(Stage primaryStage) throws Exception {
		
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppFactory.class);
        MainPresenter mainPresenter = context.getBean(MainPresenter.class);
        mainPresenter.showCustomerSearch();
        Scene scene = new Scene(mainPresenter.getView(), 800, 600);
        scene.getStylesheets().add("styles.css");
        primaryStage.setScene(scene);
        primaryStage.setTitle("First Contact - JavaFX Contact Management");
        primaryStage.show();
	}

}
