package pl.daprog.springjavafx.client.customer;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import pl.daprog.springjavafx.app.server.pub.CustomerDto;
import pl.daprog.springjavafx.app.server.pub.CustomerService;
import pl.daprog.springjavafx.client.main.MainPresenter;

public class CustomerListPresenter implements Initializable {

	@FXML
	private Node root;
	@FXML
	private TextField searchField;
	@FXML
	private ListView<CustomerDto> resultsList;

	@Inject
	private MainPresenter mainPresenter;

	@Inject 
	private CustomerService customerService;

	public void initialize(URL url, ResourceBundle resourceBundle) {
		// unfortunately FXML does not currently have a clean way to set a
		// custom CellFactory
		// like we need so we have to manually add this here in code
		resultsList
				.setCellFactory(new Callback<ListView<CustomerDto>, ListCell<CustomerDto>>() {
					public ListCell<CustomerDto> call(
							ListView<CustomerDto> CustomerDtoListView) {
						final ListCell<CustomerDto> cell = new ListCell<CustomerDto>() {
							protected void updateItem(CustomerDto customerDto,
									boolean empty) {
								super.updateItem(customerDto, empty);
								if (!empty) {
									setText(String.format("%s",
											customerDto.name));
								}
							}
						};
						cell.setOnMouseClicked(new EventHandler<Event>() {
							public void handle(Event event) {
								CustomerDto customerDto = cell.getItem();
								if (customerDto != null) {
									customerDtoSelected(customerDto.id);
								}
							}
						});
						return cell;
					}
				});
	}

	public Node getView() {
		return root;
	}

	@FXML 
	public void search(ActionEvent event) {
		String searchPhrase = searchField.getText();
		final String[] keywords = searchPhrase != null ? searchPhrase
				.split("\\s+") : null;
		final Task<List<CustomerDto>> searchTask = new Task<List<CustomerDto>>() {
			protected List<CustomerDto> call() throws Exception {
				return customerService.list(keywords);
			}
		};

		searchTask.stateProperty().addListener(
				new ChangeListener<Worker.State>() {
					public void changed(
							ObservableValue<? extends Worker.State> source,
							Worker.State oldState, Worker.State newState) {
						if (newState.equals(Worker.State.SUCCEEDED)) {
							resultsList.getItems()
									.setAll(searchTask.getValue());
						} else if (newState.equals(Worker.State.FAILED)) {
							searchTask.getException().printStackTrace();
						}
					}
				});

		new Thread(searchTask).start();
	}

	public void customerDtoSelected(Long id) {
		mainPresenter.showCustomerDetail(id); 
	 }

	@FXML 
	public void add(ActionEvent event) {
		mainPresenter.showCustomerDetail(null); 
	}
	 
}
