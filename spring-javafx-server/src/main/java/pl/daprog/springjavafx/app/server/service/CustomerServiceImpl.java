package pl.daprog.springjavafx.app.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pl.daprog.springjavafx.app.server.entity.Customer;
import pl.daprog.springjavafx.app.server.pub.CustomerDto;
import pl.daprog.springjavafx.app.server.pub.CustomerService;
import pl.daprog.springjavafx.platform.server.dao.IOperations;

@Service(value="customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private IOperations<Customer> customerDao;
    
    private void entityToDto(Customer entity, CustomerDto dto) {

    	dto.id = entity.getId();
		dto.name = entity.getName();
    }

    private void dtoToEntity(CustomerDto dto, Customer entity) {

    	if(dto.id!=null) {
    		entity.setId(dto.id);
    	}
    	entity.setName(dto.name);
    }

	public List<CustomerDto> list(final String[] keywords) {
		
		List<Customer> entities = customerDao.findAll();
		List<CustomerDto> x = new ArrayList<CustomerDto>();

		CustomerDto dto;

		for(Customer entity : entities) {
			dto = new CustomerDto();
			entityToDto(entity, dto);
			x.add(dto);
		}
		
		return x;
	}
	
	public CustomerDto find(final Long id) {

		Customer entity = customerDao.findOne(id);
		
		CustomerDto dto = new CustomerDto();
		entityToDto(entity, dto);

		return dto;
	}
	
	public CustomerDto update(final CustomerDto dto) {
		
		Customer entity = customerDao.findOne(dto.id);
		dtoToEntity(dto, entity);
		customerDao.update(entity);
		
		return dto;
		
	}

	public CustomerDto init(final CustomerDto dto) {
		
		return dto;
		
	}
	
	public Long insert(final CustomerDto dto) {
		
		Customer entity = new Customer();
		dtoToEntity(dto, entity);
		customerDao.create(entity);
		return entity.getId();
	}

}
