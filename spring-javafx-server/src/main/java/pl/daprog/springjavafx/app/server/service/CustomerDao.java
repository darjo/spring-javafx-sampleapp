package pl.daprog.springjavafx.app.server.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import pl.daprog.springjavafx.app.server.entity.Customer;
import pl.daprog.springjavafx.platform.server.dao.AbstractHibernateDao;
import pl.daprog.springjavafx.platform.server.dao.IOperations;

@Repository
public class CustomerDao extends AbstractHibernateDao<Customer> implements IOperations<Customer> {

    public CustomerDao() {
        super();
        setClazz(Customer.class);
    }

}
