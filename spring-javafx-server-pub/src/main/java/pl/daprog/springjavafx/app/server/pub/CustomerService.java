package pl.daprog.springjavafx.app.server.pub;

import pl.daprog.springjavafx.platform.server.pub.CrudService;

public interface CustomerService extends CrudService<CustomerDto> {

}
