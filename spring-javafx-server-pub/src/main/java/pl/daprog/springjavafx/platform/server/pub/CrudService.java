package pl.daprog.springjavafx.platform.server.pub;

import java.util.List;

public interface CrudService<T> {

	public List<T> list(final String[] keywords);
	
	public T find(final Long id);
	
	public T update(final T dto);

	public T init(final T dto);

	public Long insert(final T dto);
}
